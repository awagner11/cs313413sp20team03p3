package edu.luc.etl.cs313.android.shapes.model;


/**
 * A shape visitor for calculating the bounding box, that is, the smallest
 * rectangle containing the shape. The resulting bounding box is returned as a
 * rectangle at a specific location.
 */
public class BoundingBox implements Visitor<Location> {

	// TODO

	@Override
	public Location onCircle(final Circle c) {
		final int radius = c.getRadius();
		return new Location(-radius, -radius, new Rectangle(2 * radius, 2 * radius));
	}

	@Override
	public Location onFill(final Fill f) {
		return f.getShape().accept(this);
	}

	@Override
	public Location onGroup(final Group g) {
		int left=Integer.MAX_VALUE;
		int right=Integer.MIN_VALUE;
		int up =Integer.MIN_VALUE;
		int down=Integer.MAX_VALUE;

		boolean start=true;

		for(Shape shape: g.getShapes()){
			final Location l =shape.accept(this);
			final Rectangle rectangle=(Rectangle)l.getShape();

			if(start){
				left=l.getX();
				right=l.getX()+rectangle.getWidth();
				down=l.getY();
				up=l.getY() + rectangle.getHeight();
				start=false;
			}
			else {
				if (l.getX() < left) {
					left = l.getX();
				}
				if (l.getY() < down) {
					down = l.getY();
				}
				if (l.getX() + rectangle.getWidth() > right) {
					right = l.getX() + rectangle.getWidth();
				}
				if (l.getY() + rectangle.getHeight() > up) {
					up = l.getY() + rectangle.getHeight();
				}
			}


		}
		return new Location(left,down,new Rectangle(right-left,up-down));
	}

	@Override
	public Location onLocation(final Location l) {
		final int x = l.getX();
		final int y = l.getY();

		final Location newLocation = l.getShape().accept(this);
		final int a = newLocation.getX();
		final int b = newLocation.getY();
		return new Location((x + a), (y + b), newLocation.getShape());
	}

	@Override
	public Location onRectangle(final Rectangle r) {
		final int x = r.getWidth();
		final int y = r.getHeight();

		final int startX = (1/2)*x;
		final int startY = (1/2)*y;

		return new Location(-startX, -startY, r);
	}

	@Override
	public Location onStrokeColor(final StrokeColor c) {
		return c.getShape().accept(this);
	}

	@Override
	public Location onOutline(final Outline o) {
		return o.getShape().accept(this);
	}

	@Override
	public Location onPolygon(final Polygon s) {
		int left = Integer.MAX_VALUE;
		int right= Integer.MIN_VALUE;
		int up= Integer.MIN_VALUE;
		int down= Integer.MAX_VALUE;

		for(Point points: s.getPoints()){
			left=Math.min(left, points.accept(this).getX());
			right=Math.max(right, points.accept(this).getX());
			up=Math.max(up, points.accept(this).getY());
			down=Math.min(down, points.accept(this).getY());
		}
		return new Location(left, down, new Rectangle((right - left),(up - down)));
	}
}
